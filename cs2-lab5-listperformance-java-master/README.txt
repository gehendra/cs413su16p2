// Test Performance
// TODO run test and record running times for SIZE = 10, 100, 1000, 10000
// which of the two lists performs better as the size increases?

// when SIZE is 10
// It took 182ms to run TestPerformance
// public void testLinkedListAddRemove() took 92ms
// public void testArrayListAddRemove() took 69ms
// public void testLinkedListAccess() took 34ms
// public void testArrayListAccess() took 26ms


// when SIZE is 100
// It took 260ms to run TestPerformance
// public void testLinkedListAddRemove() took 77ms
// public void testArrayListAddRemove() took 97ms
// public void testLinkedListAccess() took 55ms
// public void testArrayListAccess() took 25ms


// when SIZE is 1000
// It took 1s 20ms to run TestPerformance
// public void testLinkedListAddRemove() took 71ms
// public void testArrayListAddRemove() took 355ms
// public void testLinkedListAccess() took 580ms
// public void testArrayListAccess() took 25ms


// when SIZE is 10000
// It took 10s 847ms to run TestPerformance
// public void testLinkedListAddRemove() took 82ms
// public void testArrayListAddRemove() took 3s 109ms
// public void testLinkedListAccess() took 7s 245ms
// public void testArrayListAccess() took 36ms


// LinkedList is better for adding and removing as the size grows, and ArrayList is better for accessing from the list



// TODO what happens if you use list.remove(77)?
// errors as list.remove(77) with Index out of bound as list has only 7 items and index 77 is out of bound.


// TestIterator
// Tried LinkedList and it doesn't make any difference as both of them are implementation of List interface


// TestList
// Tried LinkedList and it doesn't make any difference as both of them are implementation of List interface

// list.remove(5); // what does this method do?
// Removes element of list index 5 which is 77 and now list contain 3, 77, 4, 77, 5, 6


// list.remove(Integer.valueOf(5)); // what does this one do?
// Remove the first occurance of element in the list that has value 5

